import Layout from '../components/layout.js'
import fetch from 'isomorphic-unfetch'
import NextSeo from 'next-seo'
const linkStyle = {
  marginRight: 15
}
const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD'
}
const Post = props => (
   // console.log(props.shows)
    
  <Layout>
    <div style={layoutStyle}>
    <NextSeo config={{ title: props.shows.attributes.title, description: props.shows.attributes.field_description.value }}  />
    <h1>{props.shows.attributes.title}</h1>
    <p dangerouslySetInnerHTML={{ __html: props.shows.attributes.field_description.value }}></p>
    {/* <img src={props.show.image.medium} /> */}
    </div>
  </Layout>
)

Post.getInitialProps = async function(context) {
  const { id } = context.query
  console.log(id);
  
  const res = await fetch(`https://dev-jsonapi-drupal.pantheonsite.io/jsonapi/node/blog/${id}`)
  const show = await res.json()

  //console.log(`Fetched show: ${show}`)

  return { shows :show.data }
}

export default Post