import fetch from 'isomorphic-unfetch'
import React from "react"
import Link from 'next/link'
import Router from 'next/router';
import Layout from '../components/layout.js'
import NextSeo from 'next-seo'

const linkStyle = {
  marginRight: 15
}
const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD'
}
// import ReactQuill from 'react-quill'; // ES6
// import "react-quill/dist/quill.snow.css"; // ES6

export default class addBlog extends React.Component {
    // super(props)
    constructor(props) {
        super(props)
        if (typeof window !== 'undefined') {
          this.ReactQuill = require('react-quill')
        }
      }
     state = {
       title: "",
       routename: "",
       description: "",
       author: "",
       meta: "",
       ogtitle: "",
       ogdescription: ""
     }
     handleChange = this.handleChange.bind(this)
   
     handleChange(value) {
   
       this.setState({ description: value })
     }
   
     handleInputChange = event => {
       console.log(event);
       const target = event.target
       const value = target.value
       const name = target.name
   
       this.setState({
         [name]: value,
       })
     }
   
   
     handleSubmit = event => {
       event.preventDefault()
      // alert(`Welcome ${this.state.title} ${this.state.description} ${this.state}!`)
       fetch('https://dev-jsonapi-drupal.pantheonsite.io/jsonapi/node/blog', {
         method: 'POST',
         headers: {
           'Content-Type': 'application/vnd.api+json',
           'Accept': 'application/vnd.api+json',
           // 'Access-Control-Allow-Origin': '*'
         },
         body: JSON.stringify({
           "data": 
             {
             "type": 'node--blog',
             "attributes": {
               "title" : this.state.title,
               "body": {
                 "value": this.state.description
               },
               "field_author": this.state.author,
               "field_description":{
                 "value": this.state.description
               },
               "field_meta_tag": this.state.meta,
               "field_og_": this.state.ogtitle,
               "field_og_description": {
                 "value": this.state.ogdescription
               },
               "field_route": this.state.routename,
               "field_title": this.state.title
             }}
           
         
         })
       }).then((response) => console.log(response.json()))
         .then((responseJson) => {
           // Showing response message coming from server after inserting records.
           //console.log(responseJson);
           Router.push('http://localhost:3000/') 
             
         }).catch((error) => {
           console.log(error);
         });
     }
   
     render() {
        const ReactQuill = this.ReactQuill
       return (
         <Layout>
           <div style={layoutStyle}>
             <NextSeo config={{ title: 'Add Blog', description: 'Updated description as well' }}  />
             <link rel="stylesheet" href="//cdn.quilljs.com/1.2.6/quill.snow.css" />
          <div className="container">
           <form onSubmit={this.handleSubmit}>
           <div className="form-group col-md-12">
               <label className="form-label">Title</label>
               <input className="form-control" type="text" name="title" value={this.state.title} onChange={this.handleInputChange} />
           </div>       
            <br />
            <div className="form-group col-md-12">
              <label className="form-label"> Route Name</label>
              <input className="form-control" type="text" name="routename" value={this.state.routename} onChange={this.handleInputChange} />
            </div>
             <br />
            <div className="form-group col-md-12">
              <label className="form-label">Description</label>
              <ReactQuill defaultValue={this.state.description} onChange={this.handleChange} 
              />
            </div>
            <br />
            <div className="form-group col-md-12">
              <label className="form-label">Author Name</label>
              <input className="form-control" type="text" name="author" value={this.state.author} onChange={this.handleInputChange}
               />
            </div>
             <br />
             <div className="form-group col-md-12" >
              <label className="form-label"> Meta Tags </label>
              <input className="form-control" type="text" name="meta" value={this.state.meta} onChange={this.handleInputChange} />
             </div>
             <br />
             <div className="form-group col-md-12">
              <label className="form-label"> Og Title </label>
              <input className="form-control"
                  type="text"
                  name="ogtitle"
                  value={this.state.ogtitle}
                  onChange={this.handleInputChange}
                />
             </div>
             <br />
             <div className="form-group col-md-12">
                <label className="form-label">Og Description</label>
                <input className="form-control"
                  type="text"
                  name="ogdescription"
                  value={this.state.ogdescription}
                  onChange={this.handleInputChange}
                />
             </div>
             <br />
             <div className="col-md-12 text-center">
                <button className="btn btn-primary" type="submit">Submit</button>
             </div>
           </form>
           </div>
           </div>
         </Layout>
       )
     }
   }