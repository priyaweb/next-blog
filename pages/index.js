import Layout from '../components/layout.js'
import NextSeo from 'next-seo'

import Link from 'next/link'
import fetch from 'isomorphic-unfetch'



const linkStyle = {
    marginRight: 15
}
const layoutStyle = {
    margin: 20,
    padding: 20,
    border: '1px solid #DDD'
}

const Index = (props) => (
    <Layout>
        <div style={layoutStyle}>
        <h1>Blogs</h1>
        <ul>
            {props.shows.map(show => (
                <li key={show.id}>
                    <Link as={`/p/${show.attributes.title}`} href={`/post?id=${show.id}`}>
                        <a>{show.attributes.title}</a>
                    </Link> | 
                    <Link as={`/edit/${show.id}`} href={`/postEdit?id=${show.id}`}>
                        <a> Edit</a>
                    </Link>
                </li>
            ))}
        </ul>
        </div>
    </Layout>
)

Index.getInitialProps = async function () {
    const res = await fetch(`https://dev-jsonapi-drupal.pantheonsite.io/jsonapi/node/blog`);

    const data = await res.json()
  //  console.log(JSON.stringify(data.data));

  //  console.log(`Show data fetched. Count: ${data.data}`)

    return {
        shows: data.data.map(entry => entry)
    }
}

export default Index