import Layout from '../components/layout.js'
import NextSeo from 'next-seo'

export default function About() {
    return (
        <Layout>
            <NextSeo config={{ title: 'About us', description: 'Updated description as well' }}  />
            <p>This is the about page</p>
        </Layout>
    )
}