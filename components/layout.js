import Header from './Header'
import "../style.css"
import NextSeo from 'next-seo'
// const layoutStyle = {
//     margin: 20,
//     padding: 20,
//     border: '1px solid #DDD'
// }

const DEFAULT_SEO = {
    title: 'Blog with NextJS',
    description: 'Blog created with NextJS, the React framework',
    openGraph: {
      type: 'website',
      locale: 'en_IE',
      url: '',
      title: 'Blog',
      description: 'SEO made easy for Next.js projects',
      image:
        'https://prismic-io.s3.amazonaws.com/gary-blog%2F3297f290-a885-4cc6-9b19-3235e3026646_default.jpg',
      site_name: 'GaryMeehan.ie',
      imageWidth: 1200,
      imageHeight: 1200
    },
    twitter: {
      handle: '@garmeeh',
      cardType: 'summary_large_image'
    }
  };


const Layout = props => (
    <div>
        <NextSeo config={DEFAULT_SEO} />
    {/* <div style={layoutStyle}> */}
        <Header />
        {props.children}
    </div>
)

export default Layout