import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import Link from 'next/link';
import {
    MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavbarToggler, MDBCollapse, MDBFormInline,
    MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBContainer
} from "mdbreact";
const linkStyle ={
    color: '#000'
}
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }

    toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        return (
            <MDBNavbar color="indigo" dark expand="md">
                <MDBNavbarBrand>
                    <strong className="white-text">My Blog Site</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick={this.toggleCollapse} />
                <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                    <MDBNavbarNav left>
                        <MDBNavItem>
                            <MDBDropdown>
                                <MDBDropdownToggle nav caret>
                                    <span className="mr-2">Blog</span>
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <MDBDropdownItem href="#!">
                                        <Link href="/">
                                            <a style={linkStyle}> List</a>
                                        </Link></MDBDropdownItem>
                                    <MDBDropdownItem href="#!">
                                        <Link href="/addBlog">
                                            <a style={linkStyle}>Add Blog</a>
                                        </Link>
                                    </MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>

                        </MDBNavItem>
                        <MDBNavItem>
                            <Link href="/about">
                                <a className="nav-link">About</a>
                            </Link>
                        </MDBNavItem>

                        {/*<MDBNavItem>
                    <MDBDropdown>
                        <MDBDropdownToggle nav caret>
                        <span className="mr-2">Dropdown</span>
                        </MDBDropdownToggle>
                        <MDBDropdownMenu>
                        <MDBDropdownItem href="#!">Action</MDBDropdownItem>
                        <MDBDropdownItem href="#!">Another Action</MDBDropdownItem>
                        <MDBDropdownItem href="#!">Something else here</MDBDropdownItem>
                        <MDBDropdownItem href="#!">Something else here</MDBDropdownItem>
                        </MDBDropdownMenu>
                    </MDBDropdown>
                    </MDBNavItem>*/}
                    </MDBNavbarNav>
                    {/* <MDBNavbarNav right>
                    <MDBNavItem>
                    <MDBFormInline waves>
                        <div className="md-form my-0">
                        <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
                        </div>
                    </MDBFormInline>
                    </MDBNavItem>
                </MDBNavbarNav> */}
                </MDBCollapse>
            </MDBNavbar>
        );
    }
}

export default Header;

// import Link from 'next/link'

// const linkStyle = {
//   marginRight: 15
// }

// const Header = () => (
//   <div>
//     <Link href="/">
//       <a style={linkStyle}>Home</a>
//     </Link>
//     <Link href="/about">
//       <a style={linkStyle}>About</a>
//     </Link>
//   </div>
// )

// export default Header